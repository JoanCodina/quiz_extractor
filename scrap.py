
import  http.cookiejar, urllib
import re
top_level_url = "http://localhost/moodle/"


# The objective would be to be able to read the quiz, and generate the permutations automatically ..
# but to do so I need more information
# url = 'http://localhost/moodle/mod/quiz/attempt.php?attempt=5&cmid=82'
#
# TODO read user/Password and moodle site

# cookieJar = http.cookiejar.LWPCookieJar()
# opener = urllib.request.build_opener(
#     urllib.request.HTTPCookieProcessor(cookieJar),
#     urllib.request.HTTPRedirectHandler(),
#     urllib.request.HTTPHandler(debuglevel=0))
#
# opener.addheaders = [('User-agent', "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.1500.72 Safari/537.36")]
#
#
# forms = {"username": user ,
#          "password": passwd
#         }
#
# data = urllib.parse.urlencode(forms)
# req = urllib.request.Request(top_level_url+'login/index.php',data.encode('utf-8'))
# res = opener.open(req)
# page = res.read()
# this does not work because now Moodle includes a loginToken
filename = input("Incate the file name, it will check file with pattern_x.html from 1 to fail\n $:")
filenum=1
url = '/home/joan/Desktop/tmp/Permuta'
filename=url
while True:
    page=urllib.request.urlopen('file://'+filename+"_"+str(filenum)+".html")

    from bs4 import BeautifulSoup
    soup = BeautifulSoup(page, 'html.parser')
    #print(soup.text)
    f = open(filename+"Qiz_"+str(filenum)+".html.html", "w")
    f1 = open(filename+"Sol_"+str(filenum)+".html.html", "w")
    results = soup.find_all('div', attrs={'class':'que multichoice deferredfeedback notanswered'})
    i=1
    for result in results:
        str="<h5>  pregunta {0} </h5> <strong>".format(i)
        print(str ) ; f.write(str ) ; f1.write(str )
        str=result.find('div', attrs={'class':'qtext'}).prettify()
        print(str ) ; f.write(str ) ; f1.write(str )
        str="</strong>\n<p>respostes</p>\n"
        print(str ) ; f.write(str ) ; f1.write(str )
        # TODO, HIGHLY dependant of moodle translation
        correct=result.find('div', attrs={'class':'rightanswer'}).getText().replace("La resposta correcta és: ","")
        respostes=result.find_all('label', attrs={'class':'m-l-1'})
        correctes=0
        for resposta in respostes:
            # TODO this can fail in cases where a wrong answer contains the text of the right one,
            # like for example "yes", and "yes, but not always"
            str= resposta.prettify().replace("<label","<div").replace("</label>",'</p></div>')
            str=re.sub(r'>\n ([a-d])\.',r'>\n\1.<p style="margin-left: 40px">',str)
            print( str ) ; f.write(str )
            if correct in str:
                f1.write("<strong>"+str+"</strong>\n")
                correctes=correctes+1
            else:
                f1.write(str+"\n")
        if correctes!= 1:
            f1.write("<strong>"+result.find('div', attrs={'class':'rightanswer'}).prettify()+"</strong>\n")
        i = i + 1
    f.close()
    f1.close()
    filenum=filenum+1