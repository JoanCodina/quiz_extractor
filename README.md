# quiz_extractor

Extracts quiz from moodle pages.

Moodle includes Quiz with random queries.
You can import/create complex questions in moodle using their tools.
From the questions Moodle allows you to generate different permutations of the exam by exchanging the order of the questions and the answers (you can indicate that in the question)

But if you want to extract a given quiz from moodle is complex.
## To use this tool you must
- Generate the quiz permutations in moodle 
 - Do a quiz
 - Add the questions (fixed question set or a random set) allow randomize. 
 - Go to view the quiz
 - do not answer any question and click on end the quiz
 - Send and finish the quiz
 - Save the page into a file (firefox -save page as) with a name_PermutationNumber.html . Where PermutationNumber is 1, 2.. 3
- run the tool over the file set

The tool generates 2 files: que quiz and the solution one
The files are in very simple html, so the result can be open in browser and copied to LibreOffice or other office packages.

Just add the heading and other stuf... (it could be an improvement to have that heading already in a file to be added with the permutation number)